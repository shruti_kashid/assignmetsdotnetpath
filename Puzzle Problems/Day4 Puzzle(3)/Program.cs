﻿namespace Day3_puzzle_4
{
    class Program
    {

        public string swapString(string str,int start , int end)
        {
            char temp;
            char[] charArray = str.ToCharArray();
            temp = charArray[start];
            charArray[start] = charArray[end];
            charArray[end] = temp;
            string s = new string(charArray);
            return s;
        }
        public string permutation(string str ,int start,int end)
        {
            if(start==end)
            {
                Console.WriteLine(str);
            }
            else
            {
               for( int i=start;i<end;i++)
               {
                   str=swapString(str,start,i);
                   permutation(str,start+1,end);
                   str=swapString(str,start,i);
               }            
            }
            return str;
        }
        public static void Main(String [] agrgs)
        {
            Console.WriteLine("Enter the string ");
            string str = Console.ReadLine();
            string reverse="";
            int n=str.Length;
            Boolean result=false;
            for(int i =n-1;i>=0;i--)
            {
                reverse += str[i].ToString();
            }
            //Console.WriteLine(reverse);
            if(reverse == str)
            {
                Console.WriteLine("Its palindrome string ");

            }
            else
            {
                Console.WriteLine("It is not palindrome string ");
            }
            
            Program p=new Program();
           List<string> list=new List<string>();
           list.Add(p.permutation(str,0,n));

           for(int i=0;i<list.Count;i++)
           {
               if(list[i]==reverse)
               {
                result = true;
               }
           }
           Console.WriteLine(" Palindrome string present :  true / false "+result);
        }
    }
}