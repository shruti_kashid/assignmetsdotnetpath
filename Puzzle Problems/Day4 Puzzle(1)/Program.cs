﻿/*You have a large array with most of the elements as zero.
Use a more space-efficient data structure, SparseArray, that implements the same interface:
•	init(arr, size): initialize with the original large array and size.
•	set(i, val): updates index at i with val.
•	get(i): gets the value at index i.*/
using System;
using System.Collections.Generic;  
namespace Day4_Puzzle1
{
    class Program
    {
        public void init(List<int> sparce)
        {
              
            int temp=0,count=0;
            Console.WriteLine("enter the count of values which you want in sparce array");
            int n=Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter the elements of sparce array : ");
            for(int i=0;i<n;i++)
            {
                Console.WriteLine("Element "+i+" : ");
                temp = Convert.ToInt32(Console.ReadLine());
                
               sparce.Add(temp);
               if(sparce[i]==0)
               {
                   count++;
               } 
            }
            if(count>n-count)
            {
            Console.WriteLine("Sparce Array is : ");
            foreach(var item in sparce)
            {

                Console.Write(item+" ");
            }
            Console.ReadLine();
            }
            else
            {
                Console.WriteLine("It is not sparce array !!! It contains more no of non-zero elements ");
            }

        }
        public void set(List<int> sparce,int index1,int val)
        {
            for(int i=0;i<sparce.Count;i++)
            {
                if(i==index1)
                {
                    sparce[i]=val;
                }
            }
            Console.WriteLine("After updation Sparce Array is : ");
            foreach(var item in sparce)
            {

                Console.Write(item+" ");
            }
            Console.WriteLine();
            
        }
        public void get(List<int> sparce ,int index1)
        {
            for(int i=0;i<sparce.Count;i++)
            {
                if(i==index1)
                {
                    Console.WriteLine("Value at index "+i+" is : "+sparce[i]);
                }
            }
        }
        public static void Main(String []args)
        {
            Program p = new Program();
            List<int> tempList = new List<int>();
           int index;
            p.init(tempList);
            Console.WriteLine("Enter the index valuse which you want to update ");
            index =Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter the new number which you want to add in sparce array : ");
            int val=Convert.ToInt32(Console.ReadLine());
            p.set(tempList,index,val);
            Console.WriteLine("Enter the index value that you want to get : ");
            int val1=Convert.ToInt32(Console.ReadLine());
            p.get(tempList,val1);


        }
    }
}


























































