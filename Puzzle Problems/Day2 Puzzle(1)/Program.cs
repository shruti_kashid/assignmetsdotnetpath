﻿
/*Puzzle 3:
Given a list of integers, return the largest product that can be made by multiplying any three integers.
For example, if the list is [-10, -10, 5, 2], we should return 500, since that's -10 * -10 * 5.
You can assume the list has at least 3 positive integers.*/


using System;
using System.Collections.Generic;
namespace Day2_Puzzle_1_
{
    class Program
    {
        int findLargeProduct(List<int> value , int lengthOfList )
        {
            int LargeProduct = int.MinValue;
            for(int i=0;i<lengthOfList-2;i++)
            {
                for(int j=i+1;j<lengthOfList-1;j++)
                {
                    for(int k=j+1;k<lengthOfList;k++)
                    {
                        LargeProduct=Math.Max(LargeProduct,value[i]*value[j]*value[k]);
                    }
                }
            }
            return LargeProduct;
        }
        public static void Main(String []args)
        {
            Program p = new Program();
            List<int> temp=new List<int>(){-10, -10, 5, 2};
           int lengthOfTemp=temp.Count;
           
            Console.WriteLine( "Largest Product is " + p.findLargeProduct(temp,lengthOfTemp));
        }
    }
}
