﻿using MvcProject2.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using PagedList.Mvc;
using System.Windows.Forms;

namespace MvcProject2.Controllers
{
    public class EmployeeDataController : Controller
    {
        MVCEntities dbObj = new MVCEntities();
       
        public ActionResult EmployeeData(Employee obj)
        {
           

                return View(obj);
       
        }

        [HttpPost]
        public ActionResult AddEmployee(Employee Model)
        {
            if (ModelState.IsValid)
            {
                Employee obj = new Employee();
                obj.ID = Model.ID;
                obj.FirstName = Model.FirstName;
                obj.LastName = Model.LastName;
                obj.Email = Model.Email;
                obj.Phone = Model.Phone;
                obj.Designation = Model.Designation;
                obj.EmploymentType = Model.EmploymentType;
                obj.DepartmentType = Model.DepartmentType;
                obj.Gender = Model.Gender;
                obj.Password1 = Model.Password1;
                obj.ConfirmPassword = Model.ConfirmPassword;

                if(Model.ID ==0)
                {
                    dbObj.Employee.Add(obj);
                    dbObj.SaveChanges();
                    MessageBox.Show("Data save successfully");
                }
                else
                {
                    dbObj.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                    dbObj.SaveChanges();
                    MessageBox.Show("Data save successfully");
                }

                

            }

            ModelState.Clear();

            return View("EmployeeData");
        }
        [HttpGet]
        public ActionResult EmployeeList(string searchBy,string search,int? page)
        {

            var result =(dynamic)null;
            if (searchBy == "Gender")
            {
                result = dbObj.Employee.Where(x => x.Gender == search || search == null)
                    .ToList().ToPagedList(page ?? 1, 3);
            }
            else if (searchBy == "Name")
            {
                result = dbObj.Employee.Where(x => x.FirstName.StartsWith(search) || search == null)
                    .ToList().ToPagedList(page ?? 1, 3);
            }
            else
            {
                result = dbObj.Employee
                    .ToList().ToPagedList(page ?? 1, 3);
            }
            
            return View(result);
        }
        public ActionResult Delete(int id,int? page)
        {
            var result = dbObj.Employee.Where(x => x.ID == id).First();
            dbObj.Employee.Remove(result);
            dbObj.SaveChanges();

            var list = dbObj.Employee
                .ToList().ToPagedList(page ?? 1, 3);
            return View("EmployeeList",list);
        }
    }
}