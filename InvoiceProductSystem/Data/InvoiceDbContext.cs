﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using InvoiceProductSystem.Models;

namespace InvoiceProductSystem.Data
{
    public class InvoiceDbContext : DbContext
    {
        public InvoiceDbContext(DbContextOptions<InvoiceDbContext> options):base(options)
        {

        }
        public virtual DbSet<InvoiceTable> InvoiceTables { get; set; } = default!;

        public virtual DbSet<ProductTable> ProductTables { get; set; } = default!;

    }
}
