﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InvoiceProductSystem.Data;
using InvoiceProductSystem.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;

namespace InvoiceProductSystem.Controllers
{
    public class InvoiceController : Controller
    {
        private readonly InvoiceDbContext _context;
        private readonly IWebHostEnvironment _webHostEnvironment;
       
        public InvoiceController(InvoiceDbContext context ,IWebHostEnvironment webHostEnvironment)
        {
            _context = context;
            _webHostEnvironment = webHostEnvironment;
        }
        public IActionResult Index()
        {
            List<InvoiceTable> InvoiceTables;
            
         
            
              
            InvoiceTables = _context.InvoiceTables.ToList();
                return View(InvoiceTables);

           
            
        }

        [HttpGet]
        public IActionResult Create()
        {
            InvoiceTable invoiceTable = new InvoiceTable();
            invoiceTable.ProductTables.Add(new ProductTable() { ProductID = 1});
            return View(invoiceTable);

        }
        [HttpPost]
        public IActionResult Create(InvoiceTable invoiceTable)
        {
            //foreach(ProductTable productTable in invoiceTable.ProductTables)
            // {
            //  if(productTable.ProductName==null ||    productTable.ProductName.Length==0)
            //     invoiceTable.ProductTables.Remove(productTable);
            //}

            invoiceTable.ProductTables.RemoveAll(x => x.IsDeleted == true);
            UploadFile(invoiceTable);
           
            _context.Add(invoiceTable);
            _context.SaveChanges();
            return RedirectToAction("Index");

        }
        
      

        [HttpGet]
        public IActionResult Edit(string id)
        {
            InvoiceTable invoiceTable = _context.InvoiceTables
                .Include(e => e.ProductTables)
                .Where(a => a.InvoiceNo == id).FirstOrDefault();

            return View(invoiceTable);

           

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(InvoiceTable invoiceTable)
        {
            List<ProductTable> pdtails = _context.ProductTables.Where(d => d.InvoiceNo == invoiceTable.InvoiceNo).ToList();
            _context.ProductTables.RemoveRange(pdtails);
            _context.SaveChanges();

            invoiceTable.ProductTables.RemoveAll(x => x.IsDeleted == true);

           if(invoiceTable.Attachment !=null)
            {
                UploadFile(invoiceTable);
            }
            _context.Attach(invoiceTable);
            _context.Entry(invoiceTable).State= EntityState.Modified;
            _context.ProductTables.AddRange(invoiceTable.ProductTables);

            _context.SaveChanges();
             return RedirectToAction("Index");

        }


        public IActionResult Delete(string id)
        {
           var result = _context.InvoiceTables.Find(id);
            if(result != null)
            {
                _context.InvoiceTables.Remove(result);
                _context.SaveChanges();

            }
            return RedirectToAction("Index");
        }

        private void UploadFile(InvoiceTable invoiceTable)
        {
            if (invoiceTable.Attachment != null)
            {
                string folder = "Attachments/UploadedFiles/";
                folder += Guid.NewGuid().ToString() + "_" + invoiceTable.Attachment.FileName;
                invoiceTable.AttachmentUrl= folder;
                string serverFolder = Path.Combine(_webHostEnvironment.WebRootPath, folder);

                invoiceTable.Attachment.CopyTo(new FileStream(serverFolder, FileMode.Create));
            }
        }

        
        public IActionResult Details(string id)
        {
            InvoiceTable invoiceTable = _context.InvoiceTables
                .Include(e => e.ProductTables)
                .Where(a => a.InvoiceNo == id).FirstOrDefault();

            return View(invoiceTable);
        }


    }
}
