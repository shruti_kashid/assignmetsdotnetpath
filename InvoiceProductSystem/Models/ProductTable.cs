﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace InvoiceProductSystem.Models
{
    public class ProductTable
    {
        public ProductTable()
        {

        }

        [Key]
        public int ProductID { get; set; }

        [Required(ErrorMessage = "Ivoice no is required")]
        [ForeignKey("InvoiceTable")]
        public string InvoiceNo { get; set; } = "";

        public virtual InvoiceTable InvoiceTable { get; private set ; }

        [Required(ErrorMessage = "Product Name is required")]
        public string ProductName { get; set; } = "";

        [Required(ErrorMessage ="Description is required")]
        //[Range(1, 500, ErrorMessage = "It should have only 500 characters")]
        public string Description1 { get; set; } = "";

        [Required(ErrorMessage ="Price is required")]
        public decimal Price { get; set; }

        [Required(ErrorMessage ="Quantity is Required")]
        public decimal Quantity { get; set; }

        
        public decimal Tax { get; set; }

        [Required]
        public decimal Total { get; set; }

        [NotMapped]
        public bool IsDeleted { get; set; } = false;

    }
}
