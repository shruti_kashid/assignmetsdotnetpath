﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace InvoiceProductSystem.Models
{
    public class InvoiceTable
    {
        
        public int ID { get; set; }

        [Required(ErrorMessage = "Bill To is Required")]
        public string BillTo { get; set; } = "";
        
        [Key]
        [Required (ErrorMessage = "Invoice No is Required")]
        public string InvoiceNo { get; set; } = "";

       [Required (ErrorMessage = " Invoice Date is Required")] 
        public string InvoiceDate { get; set; } = "";

        [Required (ErrorMessage = "Due Date is Required")]
        public string DueDate { get; set; } = "";

        [Required (ErrorMessage = "Status is Required")]
        public string Status1 { get; set; } = "";
 
        public string Amount { get; set; } = "";

        public string InvoiceNote { get; set; } = "";

        public string AttachmentUrl { get; set; } = "";

        
        public decimal Subtotal1 { get; set; }

        
        public decimal TotalTax1 { get; set; }


        
        public decimal GrandTotal1 { get; set; }

        public virtual List<ProductTable> ProductTables { get; set; } = new List<ProductTable>();




        
        [NotMapped]
        public IFormFile Attachment { get; set; } 

        public enum PName
        {
            Samsung,
            Oppo,
            Motorola,
            Google,
            Relame,
            Vivo,
            Poco,
            Redmi,
            OnePlus,


        }

    }
}
