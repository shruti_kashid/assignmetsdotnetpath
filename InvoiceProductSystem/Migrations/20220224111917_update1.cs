﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace InvoiceProductSystem.Migrations
{
    public partial class update1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "GrandTotal1",
                table: "ProductTables",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "Subtotal1",
                table: "ProductTables",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "TotalTax1",
                table: "ProductTables",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "GrandTotal1",
                table: "ProductTables");

            migrationBuilder.DropColumn(
                name: "Subtotal1",
                table: "ProductTables");

            migrationBuilder.DropColumn(
                name: "TotalTax1",
                table: "ProductTables");
        }
    }
}
