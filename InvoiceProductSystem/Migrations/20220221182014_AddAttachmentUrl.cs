﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace InvoiceProductSystem.Migrations
{
    public partial class AddAttachmentUrl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AttachmentUrl",
                table: "InvoiceTables",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AttachmentUrl",
                table: "InvoiceTables");
        }
    }
}
