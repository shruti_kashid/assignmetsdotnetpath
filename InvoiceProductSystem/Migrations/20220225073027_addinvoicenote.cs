﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace InvoiceProductSystem.Migrations
{
    public partial class addinvoicenote : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "InvoiceNote",
                table: "InvoiceTables",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "InvoiceNote",
                table: "InvoiceTables");
        }
    }
}
