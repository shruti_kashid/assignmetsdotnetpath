﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm2.aspx.cs" Inherits="AspProject.WebForm2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        #form1 {
            height: 790px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
        </div>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:MVCConnectionString %>" DeleteCommand="DELETE FROM [Employee] WHERE [ID] = @ID" InsertCommand="INSERT INTO [Employee] ([FirstName], [LastName], [Email], [Phone], [Gender], [Designation], [EmploymentType], [DepartmentType]) VALUES (@FirstName, @LastName, @Email, @Phone, @Gender, @Designation, @EmploymentType, @DepartmentType)" SelectCommand="SELECT [ID], [FirstName], [LastName], [Email], [Phone], [Gender], [Designation], [EmploymentType], [DepartmentType] FROM [Employee]" UpdateCommand="UPDATE [Employee] SET [FirstName] = @FirstName, [LastName] = @LastName, [Email] = @Email, [Phone] = @Phone, [Gender] = @Gender, [Designation] = @Designation, [EmploymentType] = @EmploymentType, [DepartmentType] = @DepartmentType WHERE [ID] = @ID">
            <DeleteParameters>
                <asp:Parameter Name="ID" Type="Int32" />
            </DeleteParameters>
            
            <UpdateParameters>
                <asp:Parameter ConvertEmptyStringToNull="false" Name="FirstName" Type="String" />
                <asp:Parameter ConvertEmptyStringToNull="false" Name="LastName" Type="String" />
                <asp:Parameter ConvertEmptyStringToNull="false" Name="Email" Type="String" />
                <asp:Parameter ConvertEmptyStringToNull="false" Name="Phone" Type="String" />
                <asp:Parameter ConvertEmptyStringToNull="false" Name="Gender" Type="String" />
                <asp:Parameter ConvertEmptyStringToNull="false" Name="Designation" Type="String" />
                <asp:Parameter ConvertEmptyStringToNull="false" Name="EmploymentType" Type="String" />
                <asp:Parameter ConvertEmptyStringToNull="false"  Name="DepartmentType" Type="String" />
                <asp:Parameter Name="ID" Type="Int32" />
            </UpdateParameters>
        </asp:SqlDataSource>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="XX-Large" ForeColor="Maroon" Text="Employee Management Details"></asp:Label>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" CellPadding="3" CellSpacing="2" DataKeyNames="ID" DataSourceID="SqlDataSource1" PageSize="3" style="margin-left: 206px" Height="301px" Width="529px" OnPreRender="GridView1_PreRender" >
            <Columns>
                <asp:CommandField HeaderText="Actions" ShowDeleteButton="True" ShowEditButton="True" ButtonType="Image" EditImageUrl="~/Images/ed.png"  CancelImageUrl="~/Images/can.png" UpdateImageUrl="~/Images/ed.png" DeleteImageUrl="~/Images/del.png"/>
                <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" ReadOnly="True"  />
                <asp:TemplateField ConvertEmptyStringToNull="False" HeaderText="FirstName" SortExpression="FirstName">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox10" runat="server" Text='<%# Bind("FirstName") %>'></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="TextBox10" Font-Size="Small" ForeColor="Red" Font-Names="Arial" runat="server" ErrorMessage="First name is required"></asp:RequiredFieldValidator>
                    </EditItemTemplate>
                    <ItemTemplate>
                        
                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("FirstName") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
               
                <asp:TemplateField ConvertEmptyStringToNull="False" HeaderText="LastName" SortExpression="LastName">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("LastName") %>'></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="TextBox3" Font-Size="Small" ForeColor="Red" Font-Names="Arial" runat="server" ErrorMessage="Last name is required"></asp:RequiredFieldValidator>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label3" runat="server" Text='<%# Bind("LastName") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ConvertEmptyStringToNull="False" HeaderText="Email" SortExpression="Email">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("Email") %>'></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="TextBox4" Font-Size="Small" ForeColor="Red" Font-Names="Arial" runat="server" ErrorMessage="Email is required"></asp:RequiredFieldValidator>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label4" runat="server" Text='<%# Bind("Email") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ConvertEmptyStringToNull="False" HeaderText="Phone" SortExpression="Phone">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("Phone") %>'></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="TextBox5" Font-Size="Small" ForeColor="Red" Font-Names="Arial" runat="server" ErrorMessage="Phone is required"></asp:RequiredFieldValidator>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label5" runat="server" Text='<%# Bind("Phone") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ConvertEmptyStringToNull="False" HeaderText="Gender">
                    <EditItemTemplate>
                        <asp:DropDownList ID="DropDownList1" runat="server" SelectedValue='<%# Bind("Gender") %>'>
                            <asp:ListItem>Select Gender</asp:ListItem>
                            <asp:ListItem>Male</asp:ListItem>
                            <asp:ListItem>Female</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator InitialValue="Select Gender" ID="RequiredFieldValidator5" ControlToValidate="DropDownList1" Font-Size="Small" ForeColor="Red" Font-Names="Arial" runat="server" ErrorMessage="Gender is required"></asp:RequiredFieldValidator>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label6" runat="server" Text='<%# Bind("Gender") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ConvertEmptyStringToNull="False" HeaderText="Designation" SortExpression="Designation">
                    <EditItemTemplate>
                        <asp:DropDownList ID="DropDownList2" runat="server" SelectedValue='<%# Bind("Designation") %>'>
                            <asp:ListItem>Select Designation</asp:ListItem>
                            <asp:ListItem>SE</asp:ListItem>
                            <asp:ListItem>SSE</asp:ListItem>
                            <asp:ListItem>TL</asp:ListItem>
                            <asp:ListItem>STL</asp:ListItem>
                            <asp:ListItem>PM</asp:ListItem>
                            <asp:ListItem>VP</asp:ListItem>
                            <asp:ListItem></asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator InitialValue="Select Designation" ID="RequiredFieldValidator6" ControlToValidate="DropDownList2" Font-Size="Small" ForeColor="Red" Font-Names="Arial" runat="server" ErrorMessage="Designation is required"></asp:RequiredFieldValidator>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label7" runat="server" Text='<%# Bind("Designation") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ConvertEmptyStringToNull="False" HeaderText="EmploymentType" SortExpression="EmploymentType">
                    <EditItemTemplate>
                        <asp:DropDownList  ID="DropDownList3" runat="server" SelectedValue='<%# Bind("EmploymentType") %>'>
                            <asp:ListItem>Select Employment Type</asp:ListItem>
                            <asp:ListItem>Full Time</asp:ListItem>
                            <asp:ListItem>Part Time</asp:ListItem>
                            <asp:ListItem>Contract</asp:ListItem>
                            <asp:ListItem></asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator InitialValue="Select Employment Type" ID="RequiredFieldValidator7" ControlToValidate="DropDownList3" Font-Size="Small" ForeColor="Red" Font-Names="Arial" runat="server" ErrorMessage="Employment type is required"></asp:RequiredFieldValidator>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label8" runat="server" Text='<%# Bind("EmploymentType") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ConvertEmptyStringToNull="False" HeaderText="DepartmentType" SortExpression="DepartmentType">
                    <EditItemTemplate>
                        <asp:DropDownList ID="DropDownList4" runat="server" SelectedValue='<%# Bind("DepartmentType") %>'>
                            <asp:ListItem>Select Department Type</asp:ListItem>
                            <asp:ListItem>Engineering</asp:ListItem>
                            <asp:ListItem>HR</asp:ListItem>
                            <asp:ListItem>IT</asp:ListItem>
                            <asp:ListItem>Cloud</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator InitialValue="Select Department Type" ID="RequiredFieldValidator8" ControlToValidate="DropDownList4" Font-Size="Small" ForeColor="Red" Font-Names="Arial" runat="server" ErrorMessage="Department type is required"></asp:RequiredFieldValidator>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label9" runat="server" Text='<%# Bind("DepartmentType") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
            <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
            <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
            <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
            <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#FFF1D4" />
            <SortedAscendingHeaderStyle BackColor="#B95C30" />
            <SortedDescendingCellStyle BackColor="#F1E5CE" />
            <SortedDescendingHeaderStyle BackColor="#93451F" />
        </asp:GridView>
    &nbsp;&nbsp;&nbsp;
        <div style="margin-left: 0px">
            <br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Label ID="Count" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Medium" ForeColor="#993300" Text="Label"></asp:Label>
            <br />
            <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button ID="Button1" runat="server" BackColor="#993300" BorderColor="Black" Font-Names="Arial" Font-Size="Large" ForeColor="White" OnClick="Button1_Click" Text="Add Employee" />
        </div>
    </form>
</body>
</html>
