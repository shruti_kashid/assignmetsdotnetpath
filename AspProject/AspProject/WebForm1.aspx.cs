﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace AspProject
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (IsValid)
            {


                string ConnectionString = "Data Source=(LocalDB)\\MSSQLLocalDB;Initial Catalog=MVC;Integrated Security=True";

                SqlConnection con = new SqlConnection(ConnectionString);

                con.Open();

                string fname = TextBoxFirstName.Text;
                string lname = TextBoxLAstName.Text;
                string email = TextBoxEmail.Text;
                string phone = TextBoxPhone.Text;
                string desig = DropDownListDEsignaion.SelectedItem.Text;
                string Emptype = DropDownListEmploymentType.SelectedItem.Text;
                string Dept = DropDownListDepartment.SelectedItem.Text;
                string gender;
                if (Male.Checked)
                {
                    gender = Male.Text;
                }
                else
                {
                    gender = Female.Text;
                }
                string password2 = TextBoxPassword.Text;
                string confirmPass = TextBoxConfirmPassword.Text;

                string Query = " insert into Employee(FirstName, LastName, Email, Phone, Designation, EmploymentType, DepartmentType, Gender, Password1, ConfirmPassword) values('" + fname + "','" + lname + "','" + email + "','" + phone + "','" + desig + "','" + Emptype + "','" + Dept + "','" + gender + "','" + password2 + "','" + confirmPass + "' )";

                SqlCommand cmd = new SqlCommand(Query, con);

                cmd.ExecuteNonQuery();

                con.Close();


                MessageBox.Show("Data has been saved");
            }

             TextBoxFirstName.Text=" ";
             TextBoxLAstName.Text=" ";
             TextBoxEmail.Text=" ";
            TextBoxPhone.Text=" ";
             DropDownListDEsignaion.SelectedIndex=-1;
            DropDownListEmploymentType.SelectedIndex=-1;
            DropDownListDepartment.SelectedIndex=-1;
            Male.Checked=false;
            Female.Checked=false;




        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            TextBoxFirstName.Text = " ";
            TextBoxLAstName.Text = " ";
            TextBoxEmail.Text = " ";
            TextBoxPhone.Text = " ";
            DropDownListDEsignaion.SelectedIndex = -1;
            DropDownListEmploymentType.SelectedIndex = -1;
            DropDownListDepartment.SelectedIndex = -1;
            Male.Checked = false;
            Female.Checked = false;

        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/WebForm2.aspx");
        }
    }
}